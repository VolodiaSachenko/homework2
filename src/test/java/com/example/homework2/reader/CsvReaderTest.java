package com.example.homework2.reader;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;
import java.util.Objects;

public class CsvReaderTest {

    @BeforeAll
    static void setup() {
        System.out.println("CsvReader tests are running:");
    }

    @DisplayName("Returning value testing")
    @Test
    void readCsvFile() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource("quiz_ua.csv")).getFile());
        List<List<String>> test = CsvReader.readCsvFile(file);
        Assertions.assertFalse(test.isEmpty());
    }

    @AfterAll
    public static void done() {
        System.out.println("CsvReader tests finished");
    }

}