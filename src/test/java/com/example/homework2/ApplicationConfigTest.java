package com.example.homework2;

import lombok.Getter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@Getter
public class ApplicationConfigTest {

    private final ApplicationConfig applicationConfig = new ApplicationConfig();

    @BeforeAll
    static void setup() {
        System.out.println("ApplicationConfig tests are running:");
    }

    @DisplayName("ApplicationConfigTest not null value test")
    @Test
    void beanTest() {
        Assertions.assertNotNull(applicationConfig.quizService(applicationConfig.messageSource()));
    }

    @AfterAll
    public static void done() {
        System.out.println("ApplicationConfigTest tests finished");
    }
}