package com.example.homework2.service;

import com.example.homework2.model.Quiz;
import com.example.homework2.model.User;
import com.example.homework2.reader.CsvReader;
import lombok.Getter;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Queue;
import java.util.Scanner;
import java.util.stream.Collectors;

@Getter
@Service
public class QuizServiceImpl implements QuizService {
    private final MessageSource messageSource;
    private Queue<Quiz> quizQueue;
    private Locale locale = Locale.getDefault();
    private Resource resource;
    private int score;

    public QuizServiceImpl(ResourceBundleMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public void run() {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println(messageSource.getMessage("hello", null, locale) + "");
            System.out.println(messageSource.getMessage("select", null, locale));
            selectLanguage(input);
            mapQuiz();
            User user = identifyUser(input);
            System.out.println(messageSource.getMessage("info", null, locale) + " "
                    + Objects.requireNonNull(quizQueue.peek()).getAnswers().size() + ":");
            executeQuiz(quizQueue, input);
            System.out.println(
                    user + ", " + messageSource.getMessage("correct", null, locale) + ": " + score);
        } catch (InputMismatchException exception) {
            throw new IllegalArgumentException(messageSource.getMessage("exception", null, locale) + " "
                    + Objects.requireNonNull(quizQueue.peek()).getAnswers().size());
        }
    }

    private List<String> getAllAnswers(List<String> row) {
        return row.stream()
                .skip(2L)
                .limit(row.size() - 3L)
                .collect(Collectors.toList());
    }

    private void selectLanguage(Scanner input) {
        int language = input.nextInt();
        input.nextLine();
        if (language == 1) {
            locale = new Locale("uk_UA");
        } else if (language == 2) {
            locale = new Locale("en_UK");
        } else {
            throw new IllegalArgumentException(messageSource.getMessage("language", null, locale));
        }
    }

    private void mapQuiz() {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        if (locale.toString().equals("en_uk")) {
            this.resource = resourceLoader.getResource("quiz_en.csv");
        }
        if (locale.toString().equals("uk_ua")) {
            this.resource = resourceLoader.getResource("quiz_ua.csv");
        }
        List<List<String>> csv = new ArrayList<>();
        try {
            csv = CsvReader.readCsvFile(resource.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.quizQueue = csv.stream()
                .skip(1L)
                .map(ArrayList::new)
                .map(row -> new Quiz(Integer.parseInt(row.get(0)),
                        row.get(1), getAllAnswers(row),
                        Integer.parseInt(row.get(row.size() - 1))))
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private User identifyUser(Scanner input) {
        System.out.println(messageSource.getMessage("name", null, locale) + ":");
        String name = input.nextLine();

        System.out.println(messageSource.getMessage("surname", null, locale) + ":");
        String sureName = input.nextLine();

        return new User(name, sureName);
    }

    private void executeQuiz(Queue<Quiz> quizQueue, Scanner input) {
        while (!quizQueue.isEmpty()) {
            Quiz active = quizQueue.poll();
            System.out.println(active);
            int userAnswer = input.nextInt();
            checkAnswer(userAnswer, active.getCorrect(), active.getAnswers().size());
        }
    }

    private void checkAnswer(int answer, int correct, int maxValue) {
        if (answer < 1 || answer > maxValue) {
            throw new IllegalArgumentException(messageSource.getMessage("exception", null, locale) + " "
                    + maxValue);
        }
        if (answer == correct) {
            score++;
        }
    }
}