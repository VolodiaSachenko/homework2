package com.example.homework2.model;

import lombok.Getter;

import java.util.List;

@Getter
public class Quiz {
    private final int id;
    private final String question;
    private final List<String> answers;
    private final int correct;

    public Quiz(int id, String question, List<String> answers, int correct) {
        this.id = id;
        this.question = question;
        this.answers = answers;
        this.correct = correct;
    }

    @Override
    public String toString() {
        return id + ". " + question + "\n" + answers;
    }
}
