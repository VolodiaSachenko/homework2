package com.example.homework2;

import com.example.homework2.service.QuizServiceImpl;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        QuizServiceImpl quizService = ctx.getBean(QuizServiceImpl.class);
        quizService.run();
    }

}