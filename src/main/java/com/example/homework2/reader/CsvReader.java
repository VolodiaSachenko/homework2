package com.example.homework2.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class CsvReader {

    public static List<List<String>> readCsvFile(File file) {
        Objects.requireNonNull(file);
        List<List<String>> csv = new ArrayList<>();
        try (Scanner csvScanner = new Scanner(file)) {
            while (csvScanner.hasNextLine()) {
                csv.add(splitLine(csvScanner.nextLine()));
            }
        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
            System.err.println("File not found. Create quiz_ua.csv file here: src/main/resources/");
        }
        return csv;
    }

    private static List<String> splitLine(String line) {
        List<String> values = new ArrayList<>();
        try (Scanner csvScanner = new Scanner(line)) {
            csvScanner.useDelimiter(",");
            while (csvScanner.hasNext()) {
                values.add(csvScanner.next());
            }
        }
        return values;
    }
}
