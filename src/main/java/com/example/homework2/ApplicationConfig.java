package com.example.homework2;

import com.example.homework2.service.QuizServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class ApplicationConfig {

    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames("res", "res_en_UK", "res_uk_UA");
        source.setUseCodeAsDefaultMessage(true);
        source.setDefaultEncoding("UTF-8");
        return source;
    }

    @Bean
    public QuizServiceImpl quizService(ResourceBundleMessageSource messageSource) {
        return new QuizServiceImpl(messageSource);
    }

}